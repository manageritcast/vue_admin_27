// 导入express
const express = require('express')

// 创建app
const app = express()

app.all('*',(req,res,next)=>{
    // 设置好响应头
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");

    next()
})

// 处理浏览器发送过来的请求
app.get('/movies',(req,res)=>{
    const movies = [
        {
            id:1001,
            name:'绿皮书',
            score:8.9
        },
        {
            id:1002,
            name:'熊出没-原始时代',
            score:6.9
        },
        {
            id:1003,
            name:'流浪地球',
            score:7.9
        }
    ]

    res.json(movies)
})

// 启动
app.listen(3000,err=>{
    if (err){
        console.log(err)
    }
})