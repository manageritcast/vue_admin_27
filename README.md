# vue_admin_27

#### 介绍
深圳黑马前端27期Vue项目

#### Vue其它知识点
> 国际化 【vue-i18n】

```
实现原理：
	1、针对不同的语言设置好不同的语言包，并且写好配置文件
	
	2、在Vue组件中使用的时候，在template中使用 {{$t('语言包中定义好的名称')}}
		js中，通过 this.$t('xxx')去进行操作
		
	3、当我们需要切换成不同语言的时候，只需要更改i18n的locale

vue-i18n：
	https://kazupon.github.io/vue-i18n/

推荐文章：
	https://juejin.im/post/5aa7e18ff265da2384404334
```

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)