import Vue from 'vue'

import Vuex from './index'

const store = new Vuex.Store({
    state:{
        count:1
    },
    // getters:{
    //     getCount(state){
    //         return state.count
    //     }
    // },
    mutations:{
        add(state,payload){
            state.count += payload
        },
        substrict(state,payload){
            state.count -= payload
        }
    }
})

Vue.prototype.$store = store