export default {
    props:{
        level:{
            type:Number,
            default:2
        }    
    },
    render(h){
        return h(`h${this.level}`,this.$slots.default)
    }
}