import Vue from 'vue'

import notification from './notification/'

Vue.use(notification)

import test from './test.vue'
Vue.component('test',test)

new Vue({
    el:'#app',
    template:`
        <test></test>
    `
})