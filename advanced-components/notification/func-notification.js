import Notification from './notification'

export default {
    extends:Notification,
    computed: {
        style(){
            return {
                position:'fixed',
                right:'20px',
                bottom:`${this.verticalOffset}px`
            }
        }
    },
    mounted() {
        this.createTimer()
    },
    methods:{
        createTimer(){
            if (this.autoClose){
                this.timer = setTimeout(() => {
                    // 弹框隐藏
                    this.visible = false
                }, this.autoClose);
            }
        },
        // 清除定时器
        clearTimer(){
            if (this.timer){
                clearTimeout(this.timer)
            }
        },
        afterEnter(){
            // debugger
            this.height = this.$el.offsetHeight
        }
    },
    beforeDestroy() {
        this.clearTimer()
    },
    data(){
        return {
            verticalOffset:0,
            autoClose:3000, // 3s之后关闭弹框
            height:0,
            visible:false // 先把它设置为false，以后改成了true才会触发transition
            //才会执行afterEnter
        }
    }
}