import Notification from './notification.vue'
import notify from './function'

export default (Vue) => {
    // console.log("2222",Notification.name)
    Vue.component(Notification.name,Notification)

    Vue.prototype.$notify = notify
}

// const MyPlugin = {}
// MyPlugin.install = Vue => {
//     console.log("111111111",Notification.name)
//     Vue.component(Notification.name,Notification)
// }

// export default MyPlugin